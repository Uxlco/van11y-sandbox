import van11yHideShow from 'van11y-accessible-hide-show-aria';

export default {
  mounted() {
    this.a11y = van11yHideShow().attach(this.$el)
  }
}  